/*
	Author: EduardoQSR
	Date: 24, february, 2020
*/

import java.util.Scanner; //Importa la clase Scanner

public class Salario
{
	public static void main(String [] args)
	{
		String nombre;
		int horas;
		double pagoPorHora, pagoTotal;

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Como te llamas?");
		nombre = teclado.nextLine();
		
		System.out.println("Cuantas horas trabajaste esta semana? ");
		horas = teclado.nextInt();
		
		System.out.println("Cuanto te pagan por hora?");
		pagoPorHora = teclado.nextDouble();
		
		pagoTotal = horas * pagoPorHora;
		System.out.println("Hola " + nombre);
		System.out.println("Tu sueldo es $ " + pagoTotal);
	}
}
