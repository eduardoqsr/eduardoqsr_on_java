public class Principal
{
	public static void main(String[] args)
	{
		
		Principal.engine ((x, y) -> x + y);
		
		Principal.engine ((x, y) -> x - y);
		
		Principal.engine ((x, y) -> x / y);
		
		Principal.engine ((x, y) -> x * y);
		
		Principal.engine ((x, y) -> x % y);
		
	
	}
	//Sobrecarga de metodos
	public static int engine(CalculadoraInt cal)
	{
		int x = 2, y = 4;
		int resultado = cal.calculate(x, y);
		System.out.println("Resultado: " + resultado);
	}
	
	public static long engine(CalculadoraLong cal)
	{
		long x = 4, y = 2;
		long resultado = cal.calculate(x, y);
		System.out.println("Resultado: " + resultado);
	}
}
