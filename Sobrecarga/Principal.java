public class Principal
{
	
	public static void main(String[] args)
	{
		
		System.out.println("==> CalculadoraInt --> resultado = " + engine((int)6, (int)7).calculate(5, 5));
		System.out.println("==> CalculadoraLong --> resultado = " + engine((long)6, (long)7).calculate(5, 5));
	}
	
	public static CalculadoraInt engine(int x, int y)
	{
		return (a, b) -> x * y;
	}
	
	public static CalculadoraLong engine(long a, long b)
	{
		return (x, y) -> a - b;
	}
	
}
