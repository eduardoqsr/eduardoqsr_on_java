/*
	Author: EduardoQSR
	Date: 24, february, 2020
*/

public class LambdaTest
{
	public static void main(String[] args)
	{
		//Expresion Lambda ==> representa un objeto de una interfaz funcional
		
		//implementacion del metodo abstracto "saludar()" de la interfaz funcional
		FunctionTest ft = () -> System.out.println("Hola Mundo");
		
		//Expresiones lambda sin parámetros 1ra opcion
		System.out.println("\n\nExpresiones lambda SIN parámetros");
		ft.saludar();
		
		//Expresiones lambda sin parámetros 2da opcion
		LambdaTest objeto = new LambdaTest();
		objeto.miMetodo(ft);
		
		
		
		
		//Expresiones lambda con parámetros opcion 1
		System.out.println("Expresiones lambda CON parámetros opcion 1: ");
		Operaciones op = (num1, num2) -> System.out.println(num1 + num2);
		op.imprimeSuma(5, 10);
		
		//Expresiones lambda con parámetros opcion 2
		System.out.println("Expresiones lambda CON parámetros opcion 2: ");
		LambdaTest obj = new LambdaTest();
		obj.miMetodoDos(op, 10, 10);
		//Expresiones lambda con parámetros opcion 3
		System.out.println("Expresiones lambda CON parámetros opcion 3: ");
		obj.miMetodoTres((num1, num2) ->  System.out.println(num1 - num2), 20, 10);
		obj.miMetodoTres((num1, num2) ->  System.out.println(num1 * num2), 20, 10);
	}
	
	public void miMetodo(FunctionTest parametro)
	{
		parametro.saludar();
	}
	
	public void miMetodoDos(Operaciones op, int num1, int num2)
	{
		op.imprimeSuma(num1, num2);
	}
	
	public void miMetodoTres(Operaciones op, int num1, int num2)
	{
		op.imprimeSuma(num1, num2);
	}
}
